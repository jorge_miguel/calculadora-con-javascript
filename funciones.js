var a;
var b;
var operacion;

function inicio(){
	
	var pantalla = document.getElementById('pantalla');
	var reset = document.getElementById('reset');
	var suma = document.getElementById('suma');
	var resta = document.getElementById('resta');
	var multiplicacion = document.getElementById('multiplicacion');
	var division = document.getElementById('division');
	var igual = document.getElementById('igual');
	var uno = document.getElementById('uno');
	var dos = document.getElementById('dos');
	var tres = document.getElementById('tres');
	var cuatro = document.getElementById('cuatro');
	var cinco = document.getElementById('cinco');
	var seis = document.getElementById('seis');
	var siete = document.getElementById('siete');
	var ocho = document.getElementById('ocho');
	var nueve = document.getElementById('nueve');
	var cero = document.getElementById('cero');

	
	uno.onclick = function(click){
  		pantalla.textContent = pantalla.textContent  + "1";
	}
	dos.onclick = function(click){
  		pantalla.textContent = pantalla.textContent  + "2";
	}
	tres.onclick = function(click){
  		pantalla.textContent = pantalla.textContent  + "3";
	}
	cuatro.onclick = function(click){
  		pantalla.textContent = pantalla.textContent  + "4";
	}
	cinco.onclick = function(click){
  		pantalla.textContent = pantalla.textContent  + "5";
	}
	seis.onclick = function(click){
  		pantalla.textContent = pantalla.textContent  + "6";
	}
	siete.onclick = function(click){
  		pantalla.textContent = pantalla.textContent  + "7";
	}
	ocho.onclick = function(click){
  		pantalla.textContent = pantalla.textContent  + "8";
	}
	nueve.onclick = function(click){
  		pantalla.textContent = pantalla.textContent  + "9";
	}
	cero.onclick = function(click){
  		pantalla.textContent = pantalla.textContent  + "0";
	}
	reset.onclick = function(click){
  		resetear();
	}
	suma.onclick = function(click){
  		a = pantalla.textContent;
  		operacion = "+";
  		limpiar();
	}
	resta.onclick = function(click){
  		a = pantalla.textContent;
  		operacion = "-";
  		limpiar();
	}
	multiplicacion.onclick = function(click){
  		a = pantalla.textContent;
  		operacion = "*";
  		limpiar();
	}
	division.onclick = function(click){
  		a = pantalla.textContent;
  		operacion = "/";
  		limpiar();
	}
	igual.onclick = function(click){
  		b = pantalla.textContent;
  		resolver();
	}
}


function limpiar(){
	pantalla.textContent = "";
}

function resetear(){
	pantalla.textContent = "";
	a = 0;
	b = 0;
	operacion = "";
}

function resolver(){
	var res = 0;
	switch(operacion){
		case "+":
			res = parseFloat(a) + parseFloat(b);
			break;

		case "-":
		    res = parseFloat(a) - parseFloat(b);
		    break;

		case "*":
			res = parseFloat(a) * parseFloat(b);
			break;

		case "/":
			res = parseFloat(a) / parseFloat(b);
			break;
	}
	resetear();
	pantalla.textContent = res;
}
